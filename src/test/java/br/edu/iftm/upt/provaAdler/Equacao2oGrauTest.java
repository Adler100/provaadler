package br.edu.iftm.upt.provaAdler;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Equacao2oGrauTest {

	@Test
	public void raizUnica() {
		Equacao2oGrau eq = new Equacao2oGrau(4, -4, 1);
		assertEquals(eq.getRaizes().getR1(), 0.5, 0.0001);
		assertEquals(eq.getRaizes().getR2(), 0.5, 0.0001);
	}
	
	
	@Test
	public void RaizesUnica() {
		Equacao2oGrau equacao = new Equacao2oGrau(1, 8, 16);
		assertEquals(equacao.getRaizes().getR1(), -4);
		assertEquals(equacao.getRaizes().getR2(), -4);
	}
	@Test
	public void DuasRaizes() {
		Equacao2oGrau eq = new Equacao2oGrau(1, -2, -3);
		assertEquals(eq.getRaizes().getR1(), 3);
		assertEquals(eq.getRaizes().getR2(), -1);
		Equacao2oGrau eq2 = new Equacao2oGrau(3, -7, 4);
		assertEquals(eq2.getRaizes().getR1(), 1.3333, 0.0001);
		assertEquals(eq2.getRaizes().getR2(), 1);
		
	}
	
	
	
	@Test
	public void raizesDaEquacao(){
		Raizes raizes = new Raizes(1, 2);
		assertEquals(raizes.getR1(), 1);
		assertEquals(raizes.getR2(), 2);
	}
	
	@Test
	public void deltaNegativo() {
		Equacao2oGrau eq = new Equacao2oGrau(5, 1, 6);
		assertThrows(ArithmeticException.class,()->eq.getRaizes());
		Equacao2oGrau eq2 = new Equacao2oGrau(5, 1, 6);
		assertThrows(ArithmeticException.class,()->eq2.getRaizes());
	}

}

